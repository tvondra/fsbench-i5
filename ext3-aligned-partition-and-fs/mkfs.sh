mkfs.ext3 -E stride=128,stripe_width=128 /dev/sda4


bench ~ # mkfs.ext3  -E stride=128,stripe_width=128 /dev/sda4
mke2fs 1.42.12 (29-Aug-2014)
/dev/sda4 contains a ext3 file system
	last mounted on Sat May 23 14:22:15 2015
Proceed anyway? (y,n) y
Discarding device blocks: done
Creating filesystem with 22290758 4k blocks and 5578752 inodes
Filesystem UUID: e94d768a-ad49-4785-adf9-b99f8efc06cb
Superblock backups stored on blocks:
	32768, 98304, 163840, 229376, 294912, 819200, 884736, 1605632, 2654208,
	4096000, 7962624, 11239424, 20480000

Allocating group tables: done
Writing inode tables: done
Creating journal (32768 blocks): done
Writing superblocks and filesystem accounting information: done
