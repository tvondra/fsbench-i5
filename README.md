btrfs-aligned-partition

* partition aligned to erase block (1024)
* filesystem created with defaults (no options)
* mounted with defaults + noatime

btrfs-aligned-partition-nobarrier-discard

* partition aligned to erase block
* filesystem created with defaults (no options)
* mounted with defaults + noatime + nobarrier + discard

btrfs-aligned-partition-ssd

* partition aligned to erase block
* filesystem created with defaults (no options)
* mounted with defaults + noatime + ssd

btrfs-aligned-partition-ssd-nobarrier

* partition aligned to erase block
* filesystem created with defaults (no options)
* mounted with defaults + noatime + ssd + nobarrirer

btrfs-aligned-partition-ssd-nobarrier-discard

* partition aligned to erase block
* filesystem created with defaults (no options)
* mounted with defaults + noatime + ssd + nobarrier + discard

btrfs-aligned-partition-ssd-nobarrier-discard-fpw

* partition aligned to erase block
* filesystem created with defaults (no options)
* mounted with defaults + noatime + ssd + nobarrier + discard
* full_page_writes=off (BTRFS protects against torn pages)

btrfs-aligned-partition-ssd-nobarrier-discard-lzo

* partition aligned to erase block
* filesystem created with defaults (no options)
* mounted with defaults + noatime + ssd + nobarrier + discard + compress=lzo

btrfs-aligned-partition-ssd-nobarrier-discard-nodatacow

* partition aligned to erase block
* filesystem created with defaults (no options)
* mounted with defaults + noatime + ssd + nobarrier + discard + nodatacow

btrfs-aligned-partition-ssd-nobarrier-discard-space-cache

* partition aligned to erase block
* filesystem created with defaults (no options)
* mounted with defaults + noatime + ssd + nobarrier + discard + space_cache

btrfs-aligned-partition-ssd-nobarrier-discard-ssd-spread

* partition aligned to erase block
* filesystem created with defaults (no options)
* mounted with defaults + noatime + ssd + nobarrier + discard + ssd_spread

btrfs-aligned-partition-ssd-nobarrier-discard-zlib

* partition aligned to erase block
* filesystem created with defaults (no options)
* mounted with defaults + noatime + ssd + nobarrier + discard + compress=zlib

btrfs-unaligned

* partition not aligned to erase block
* filesystem created with defaults (no options)
* mounted with defaults + noatime

ext3-aligned-partition

* partition aligned to erase block
* ext3 created with defaults (no options)
* mounted with defaults + noatime

ext3-aligned-partition-and-fs

* partition aligned to erase block
* ext3 created with 128*4KB (512KB) stride
* mounted with defaults + noatime

ext3-unaligned

* partition not aligned to erase block
* ext3 created with defaults (no options)
* mounted with defaults + noatime

ext4-aligned-partition

* partition aligned to erase block
* ext4 created with defaults (no options)
* mounted with defaults + noatime

ext4-aligned-partition-and-fs

* partition aligned to erase block
* ext4 created with 128*4KB (512KB) stride
* mounted with defaults + noatime

ext4-aligned-partition-and-fs-and-discard

* partition aligned to erase block
* ext4 created with 128*4KB (512KB) stride
* mounted with defaults + noatime + discard

ext4-aligned-partition-and-fs-and-discard-deadline

* partition aligned to erase block
* ext4 created with 128*4KB (512KB) stride
* mounted with defaults + noatime + discard
* i/o scheduler (deadline)

ext4-aligned-partition-and-fs-and-discard-noop

* partition aligned to erase block
* ext4 created with 128*4KB (512KB) stride
* mounted with defaults + noatime + discard
* i/o scheduler (deadline)

ext4-aligned-partition-and-fs-and-nobarrier

* partition aligned to erase block
* ext4 created with 128*4KB (512KB) stride
* mounted with defaults + noatime + nobarrier

ext4-aligned-partition-and-fs-and-nobarrier-discard

* partition aligned to erase block
* ext4 created with 128*4KB (512KB) stride
* mounted with defaults + noatime + nobarrier + discard

ext4-aligned-partition-and-fs-and-nobarrier-discard-deadline

* partition aligned to erase block
* ext4 created with 128*4KB (512KB) stride
* mounted with defaults + noatime + nobarrier + discard
* i/o scheduler (deadline)

ext4-aligned-partition-and-fs-and-nobarrier-discard-noop

* partition aligned to erase block
* ext4 created with 128*4KB (512KB) stride
* mounted with defaults + noatime + nobarrier + discard
* i/o scheduler (noop)

ext4-unaligned

* partition not aligned to erase block
* ext4 created with defaults (no options)
* mounted with defaults + noatime

f2fs-aligned

* partition aligned to erase block
* mounted with defaults + noatime

f2fs-aligned-nobarrier

* partition aligned to erase block
* mounted with defaults + noatime + nobarrier

f2fs-aligned-nobarrier-discard

* partition aligned to erase block
* mounted with defaults + noatime + nobarrier + discard

f2fs-unaligned

* partition not aligned to erase block
* mounted with defaults + noatime

reiserfs-36-barrier-enable

* partition aligned to erase block
* mounted with defaults + noatime + barrier=flush

reiserfs-36-barrier-none

* partition aligned to erase block
* mounted with defaults + noatime + barrier=none

xfs-aligned-partition

* partition aligned to erase block
* mounted with defaults + noatime

xfs-aligned-partition-and-fs

* partition aligned to erase block
* xfs created with 512KB stride
* mounted with defaults + noatime

xfs-aligned-partition-and-fs-and-nobarrier

* partition aligned to erase block
* xfs created with 512KB stride
* mounted with defaults + noatime + nobarrier

xfs-aligned-partition-and-fs-and-nobarrier-discard

* partition aligned to erase block
* xfs created with 512KB stride
* mounted with defaults + noatime + nobarrier + discard

xfs-unaligned

* partition not aligned to erase block
* xfs created defaults
* mounted with defaults + noatime

zfs-aligned

* partition aligned to erase block

zfs-aligned-ashift-12

* partition aligned to erase block
* zfs created with ashift=12 (4kB)

zfs-aligned-ashift-13

* partition aligned to erase block
* zfs created with ashift=13 (8kB)

zfs-aligned-ashift-13-recordsize-8k

* partition aligned to erase block
* zfs created with ashift=13 and recordsize=8kB

zfs-aligned-ashift-13-recordsize-8k-logbias

* partition aligned to erase block
* zfs created with ashift=13 and recordsize=8kB and logbias=throughput

zfs-aligned-ashift-13-recordsize-8k-logbias-compression

* partition aligned to erase block
* zfs created with ashift=13 and recordsize=8kB and logbias=throughput and compression
