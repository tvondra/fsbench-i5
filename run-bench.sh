PATH=/home/postgres/pg-9.4.1/bin:$PATH

sudo smartctl -a /dev/sda > smartctl.start

# collect info
sudo fdisk -lu /dev/sda > fdisk.log
mount > mount.log

pg_ctl -D /mnt/data/pgdata init > init.log 2>&1

cp ../postgresql.conf /mnt/data/pgdata/

pg_ctl -D /mnt/data/pgdata -l pg.log start

sleep 5

createdb pgbench

../pgbench-auto.py --clients 1 2 4 8 16 --repeats 3 --duration 1800 -d pgbench --no-memcheck > bench.log 2>&1

pg_ctl -D /mnt/data/pgdata -l pg.log stop

sudo smartctl -a /dev/sda > smartctl.stop
