#!/usr/bin/python

import argparse
import math
import os
import re
import subprocess

from datetime import datetime
from multiprocessing import cpu_count

# global parameters, because I'm lazy
dbname  = None
host    = None

def parse_arguments():
    'defines and parses the command line arguments'

    parser = argparse.ArgumentParser(description='pgbench-auto argument parser')

    parser.add_argument('-o', '--output', action='store', default=None, help='output directory (default: ./results-YYYYMMDD-HHMM)')

    parser.add_argument('--clients', metavar='C', type=int, nargs='*', default=None, help='number of clients to test (default: derived from #cpu)')
    parser.add_argument('--duration', type=int, action='store', default=600, help='length of a run (default: 600 seconds)')
    parser.add_argument('--repeats', type=int, action='store', default=3, help='number of run repetitions (default: 3)')

    parser.add_argument('-H', '--host', action='store', default='localhost', help='database host (default: localhost)')
    parser.add_argument('-d', '--dbname', action='store', default='pgbench', help='database name (default: pgbench)')
    parser.add_argument('-u', '--user', action='store', default='postgres', help='user name (default: postgres)')

    parser.add_argument('--no-small', action='store_true', default=False, help='disable small scale (150MB)')
    parser.add_argument('--no-medium', action='store_true', default=False, help='disable medium scale (25%% RAM)')
    parser.add_argument('--no-large', action='store_true', default=False, help='disable large scale (200%% RAM)')
    parser.add_argument('--no-memcheck', action='store_true', default=False, help='skip check of RAM amount (expected ~64GB)')

    return parser.parse_args()

def log(msg):
    'a simple logging function (just a timestamp and the message)'
    print datetime.now(),':',msg

def get_total_memory():
    'returns amount of available system RAM (in kilobytes)'

    # read the info from /proc/meminfo directly (fuck Windows!)
    with open('/proc/meminfo') as f:
        for line in f:

            # split the line into pieces
            values = re.split('\s+', line.strip())

            # look for MemTotal key, expect kB
            if (values[0] == 'MemTotal:') and (values[2] == 'kB'):
                return int(values[1])

    return None

def round_scale(scale):
    'rounds scale in a reasonable way, e.g. 1234 => 1300, 123 => 130 etc.'

    l = math.floor(math.log(scale,10))-1
    x = math.pow(10,l)
    return int(math.ceil(scale / x) * x)

def get_pgbench_scales(no_small, no_medium, no_large, memory = get_total_memory()):
    'returns scales for small/medium/large pgbench runs'

    scales = {}

    if not no_small:
        scales.update({'small' : 10})

    if not no_medium:
        scales.update({'medium' : round_scale(memory * 0.25 / (15 * 1024))})

    if not no_large:
        scales.update({'large' : round_scale(memory * 2 / (15 * 1024))})

    return scales

def get_pgbench_clients(ncpus = cpu_count()):
    'returns number of clients for pgbench scalability tests (derived from number of cpus)'

    c = 1
    clients = []

    while (c <= (2*ncpus)):

        clients.append(c)

        if (c < 4):
            c += 1
        elif (c < 16):
            c += 4
        elif (c < 32):
            c += 8
        else:
            c += 16

    return clients

def count_runs(scales, clients, repeats):
    'count number of pgbench runs (both r/o and r/w at the same time)'

    return len(scales) * len(clients) * repeats * 2

def estimate_runtime(scales, clients, repeats, duration):
    'estimates duration of the tests (both for r/o and r/w workloads)'

    return count_runs(scales, clients, repeats) * duration

def print_benchmark_params(scales, clients, repeats, duration, outdir):
    'print the basic benchmark parameters (scales, ...)'

    # compute the estimate
    est_runtime = estimate_runtime(scales, clients, repeats, duration)

    print "output directory:",outdir
    print "clients:",(" ".join([str(c) for c in clients]))
    print "scales:",(" ".join([str(scales[k]) for k in scales]))
    print "repeats:",repeats
    print "test runtime (seconds):",duration
    print "test runs:", count_runs(scales, clients, repeats)
    print "estimated total runtime (seconds):",est_runtime
    print "estimated total runtime (hours):",round(est_runtime/60.0/60.0,1)

def run_benchmark(scales, clients, repeats, duration, readonly=False):
    'runs a r/o or r/w benchmark - all scales, client counts, repeats'

    for s in scales:

        log("running %s for '%s' scale" % ('r/o' if readonly else 'r/w', s))

        scaledir = "%(type)s/%(scale)s" % {
            'scale' : s,
            'type' : ('ro' if readonly else 'rw')
        }

        os.makedirs(scaledir)

        run_init(scales[s], outdir=scaledir)

        run_warmup(max(clients), duration, outdir=scaledir)

        for run in range(repeats):

            # directory for results (this run)
            run_dir = "%(scaledir)s/%(run)d" % {'scaledir' : scaledir, 'run' : (run+1)}

            os.makedirs(run_dir)

            log("running benchmark round %d (dir=%s)" % ((run+1), run_dir))

            for c in clients:
                run_pgbench(c, c, duration, readonly, outdir=run_dir)
                merge_transaction_logs("%s/%d.translog" % (run_dir, c))

def run_command(cmd, stdout, stderr):
    'runs the command and calls exit in case of error'

    try:

        r = subprocess.call(cmd, stdout=stdout,  stderr=stderr)

        # check return value of the command
        if r != 0:
            raise Exception("command failed with exit code %d" % (r,))

    except Exception as ex:
        log("ERROR: command failed: %s" % (str(cmd),))
        log("ERROR: exception '%s'" % (str(ex),))

        # just die horrible death
        exit(10)

def dump_pg_settings():

    log("fetching pg_settings")

    out = open('settings.txt',  'w')

    run_command(['psql', '-d', dbname, '-h', host, '-c', 'SELECT * FROM pg_settings'], out, out)

def run_init(scale, outdir):

    log("running pgbench init for scale %d" % (scale,))

    out = open('%s/init.log' % (outdir,),  'w')
    err = open('%s/init.err' % (outdir,),  'w')

    try:
        run_command(['dropdb', 'pgbench'], out, err)
    except:
        pass

    run_command(['createdb', 'pgbench'], out, err)

    run_vacuum(outdir=outdir)

    run_command(['pgbench', '-i', '-s', str(scale), '-d', dbname, '-h', host], out, err)

def run_vacuum(outdir):

    log("running vacuumdb")

    out = open('%s/vacuum.log' % (outdir,),  'w')
    err = open('%s/vacuum.err' % (outdir,),  'w')

    run_command(['vacuumdb', '-a', '-f', '-z'], out, err)
    run_command(['vacuumdb', '-a', '-F', '-z'], out, err)

def run_warmup(nclients, duration, readonly=False, outdir="./"):

    log("running pgbench warmup with %d clients" % (nclients,))

    out = open('%s/warmup.log' % (outdir,),  'w')
    err = open('%s/warmup.err' % (outdir,),  'w')

    if readonly:
        run_command(['pgbench', '-S', '-h', host, '-c', str(nclients), '-j', str(nclients), '-T', str(duration), dbname], out,  err)
    else:
        run_command(['pgbench', '-h', host, '-c', str(nclients), '-j', str(nclients), '-T', str(duration), dbname], out,  err)

def run_pgbench(nclients, nthreads, duration, readonly=False, outdir="./"):

    log("running pgbench clients=%d threads=%d duration=%d readonly=%s" % (nclients, nthreads, duration, str(readonly)))

    out = open('%s/%d.log' % (outdir, nclients), 'w')
    err = open('%s/%d.err' % (outdir, nclients), 'w')

    run_command(['psql', '-c', 'checkpoint', '-h', host, dbname], out, err)

    if readonly:
        run_command(['pgbench', '-S', '-h', host, '-c', str(nclients), '-j', str(nthreads), '-T', str(duration), '-l',
                        '--aggregate-interval', str(1), dbname], out,  err)
    else:
        run_command(['pgbench', '-h', host, '-c', str(nclients), '-j', str(nthreads), '-T', str(duration), '-l',
                        '--aggregate-interval', str(1), dbname], out,  err)

def parse_results(scales, runs, clients):
    'reads and parses the results into an array'

    results = {}

    for t in ['ro', 'rw']:

        results.update({t : {}})

        for s in scales:

            results[t].update({s : {}})

            for c in clients:

                results[t][s].update({c : []})

                for r in range(runs):

                    with open('%s/%s/%d/%d.log' % (t, s, r+1, c), 'r') as f:

                        for l in f:

                            r = re.match('tps = (.*) \(including.*', l)
                            if r:
                                results[t][s][c].append(float(r.group(1)))

    return results

def print_aggregated_results(results):

    first = True

    for t in results:
        for s in results[t]:

            x = [t, s]
            tps = []
            clients = []
            cl = sorted([int(c) for c in results[t][s].keys()])
            for c in cl:
                clients.append(c)
                tps.append(int(sum(results[t][s][c]) / len(results[t][s][c])))

            x.extend(tps)

            if first:
                z = ["type", "scale"]
                z.extend(cl)
                print ("%-10s%-10s" + ("%10s" * len(tps))) % tuple(z)
                print "-" * len(("%-10s%-10s" + ("%10s" * len(tps))) % tuple(z))
                first = False

            fmt = ("%-10s%-10s" + ("%10s" * len(tps)))
            print fmt % tuple(x)

    print ""

def print_aggregated_results_csv(results):

    first = True

    with open('aggregated.csv', 'w') as outfile:

        for t in results:
            for s in results[t]:

                x = [t, s]
                tps = []
                clients = []
                cl = sorted([int(c) for c in results[t][s].keys()])
                for c in cl:
                    clients.append(c)
                    tps.append(int(sum(results[t][s][c]) / len(results[t][s][c])))

                x.extend(tps)

                if first:
                    first = False
                    z = ["type", "scale"]
                    z.extend(cl)
                    z = [str(v) for v in z]
                    outfile.write(",".join(z) + "\n")

                fmt = ("%-10s%-10s" + ("%10s" * len(tps)))
                x = [str(v) for v in x]
                outfile.write(",".join(x) + "\n")

def print_raw_results(results):

    first = True

    for t in results:
        for s in results[t]:
            for c in sorted(results[t][s].keys()):

                if first:
                    z = ["type", "scale", "clients"]
                    z.extend(range(len(results[t][s][c])))

                    print ("%-10s%-10s%-10s" + ("%10s" * len(results[t][s][c]))) % tuple(z)
                    print "-" * len(("%-10s%-10s%-10s" + ("%10s" * len(results[t][s][c]))) % tuple(z))

                    first = False

                z = [t, s, c]
                z.extend([int(v) for v in results[t][s][c]])

                fmt = ("%-10s%-10s%-10s" + ("%10s" * len(results[t][s][c])))
                print fmt % tuple(z)

    print ""

def print_raw_results_csv(results):

    first = True

    with open('raw.csv', 'w') as outfile:

        for t in results:
            for s in results[t]:
                for c in sorted(results[t][s].keys()):

                    if first:
                        first = False
                        z = ["type", "scale", "clients"]
                        z.extend(range(len(results[t][s][c])))
                        z = [str(v) for v in z]
                        outfile.write(",".join(z) + "\n")

                    z = [t, s, c]
                    z.extend([str(v) for v in results[t][s][c]])
                    z = [str(v) for v in z]
                    outfile.write(",".join(z) + "\n")

def merge_arrays(a, b):

    # return the longer array
    if len(a) != len(b):
        return a if (len(a) > len(b)) else b

    c = [0] * len(a)

    # proper merging of the columns
    c[0] = a[0] + b[0]
    c[1] = a[1] + b[1]
    c[2] = a[2] + b[2]
    c[3] = a[3] if (a[3] < b[3]) else b[3]
    c[4] = a[4] if (a[4] > b[4]) else b[4]

    return c

def remove_transaction_logs():
    'deletes transaction logs forgotten from previous runs'

    for f in os.listdir('./'):
        if re.match('^pgbench_log\.[0-9]+', f):
            os.unlink(f)

def merge_transaction_logs(outfile):
    'merges the transaction logs produced by each thread separately'

    for f in os.listdir('./'):
        if re.match('^pgbench_log\.[0-9]+$', f):

            files = [f]

            # process the first transaction log (no segment number)

            transactions = {}
            with open(f, 'r') as ifile:

                for l in ifile:
                    tmp = l.strip().split(" ")
                    tmp = [int(v) for v in tmp]

                    # skip lines with invalid number of columns
                    if len(tmp) != 6:
                        continue

                    transactions.update({tmp[0] : tmp[1:]})

            seqno = 1
            while os.path.isfile("%s.%d" %(f, seqno)):

                files.append("%s.%d" %(f, seqno))

                with open("%s.%d" % (f, seqno), 'r') as ifile:

                    # merge it into the first transaction log (ignore timestamps missing there)
                    for l in ifile:

                        # split into columns, cast to integers
                        tmp = l.strip().split(" ")
                        tmp = [int(v) for v in tmp]

                        # skip lines with invalid number of columns
                        if len(tmp) != 6:
                            continue

                        if int(tmp[0]) in transactions:
                            transactions.update({int(tmp[0]) : merge_arrays(tmp[1:], transactions[int(tmp[0])])})

                seqno += 1

            # pack the transaction logs into a tgz archive
            cmd = ['tar', '-czf', '%s.tgz' % (outfile,)]
            cmd.extend(files)
            run_command(cmd, None, None)

            # unlink all the transaction files
            for tmp in files:
                os.unlink(tmp)

            if len(transactions) != 0:

                with open(outfile, 'w') as ofile:
                    for ts in sorted(transactions.keys()):
                        t = [str(t) for t in transactions[ts]]
                        ofile.write(str(ts) + " " + " ".join(t) + "\n")

if __name__ == '__main__':

    args = parse_arguments()

    # test parameters
    scales = get_pgbench_scales(args.no_small, args.no_medium, args.no_large)

    clients = args.clients
    if not clients:
        clients = get_pgbench_clients()

    repeats = args.repeats
    duration = args.duration

    if args.output is None:
        args.output = datetime.now().strftime('results-%Y%m%d-%H%M')

    # print some benchmarking info first
    print_benchmark_params(scales, clients, repeats, duration, args.output)

    # some basic sainty checks - nonempty clients/scales
    if len(scales) == 0:
        print "ERROR: empty list of test scales"
        exit(1)

    if len(clients) == 0:
        print "ERROR: empty list of clients"
        exit(2)

    # we expect memory between 63GB and 65GB
    ram = round(get_total_memory()/1024.0/1024.0)
    if (ram not in (63,64,65)) and (not args.no_memcheck):
        print "WARNING: incorrect amount of total RAM (expected ~64GB, got %d)" % (ram,)
        exit(3)

    # copy the parameters into global variables (messy)
    dbname = args.dbname
    host   = args.host

    # check the existence of output directory, create it
    if os.path.exists(args.output):
        print "ERROR: directory %s already exists" % (args.output, )
        exit(4)

    os.makedirs(args.output)

    # skip to the work directory
    os.chdir(args.output)

    # dump postgres settings, in case we need that
    dump_pg_settings()

    # run read-only, then read-write benchmarks
    run_benchmark(scales, clients, repeats, duration, True)
    run_benchmark(scales, clients, repeats, duration, False)

    # parse the results (read log files, ...)
    results = parse_results(scales, repeats, clients)

    # print raw results first, then aggregated (average of runs)
    print_raw_results(results)
    print_aggregated_results(results)

    # print results in CSV format (both aggregated and raw)
    print_raw_results_csv(results)
    print_aggregated_results_csv(results)

